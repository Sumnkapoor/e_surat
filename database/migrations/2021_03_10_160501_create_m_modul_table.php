<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMModulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::dropIfExists('m_modul');
        Schema::create('m_modul', function (Blueprint $table) {
            $table->bigIncrements('modul_id');
           	$table->string('nama_modul');
            $table->timestamp('dt_record')->nullable();
			$table->string('user_record');
			$table->timestamp('dt_modified')->nullable();
			$table->string('user_modified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_modul');
    }
}

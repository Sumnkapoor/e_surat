<?php

use Illuminate\Database\Seeder;

class m_sandiTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sandi_transaksi')->delete();
		  DB::table('m_sandi_transaksi')->insert([
		    [ 'id' => 1, 'kode_transaksi_kliring' => "01", 'nama_transaksi_kliring' => "Warkat BL", 'id_jenis_transaksi' => 1, 'keterangan' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode_transaksi_kliring' => "02", 'nama_transaksi_kliring' => "CN STIEP", 'id_jenis_transaksi' => 2, 'keterangan' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode_transaksi_kliring' => "03", 'nama_transaksi_kliring' => "DN STIEP", 'id_jenis_transaksi' => 1, 'keterangan' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode_transaksi_kliring' => "04", 'nama_transaksi_kliring' => "Tolakan Keluar", 'id_jenis_transaksi' => 1, 'keterangan' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode_transaksi_kliring' => "11", 'nama_transaksi_kliring' => "Warkat STIEP", 'id_jenis_transaksi' => 2, 'keterangan' => 2, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode_transaksi_kliring' => "12", 'nama_transaksi_kliring' => "CN Bank Lain", 'id_jenis_transaksi' => 1, 'keterangan' => 2, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode_transaksi_kliring' => "13", 'nama_transaksi_kliring' => "DN Bank Lain", 'id_jenis_transaksi' => 2, 'keterangan' => 2, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 8, 'kode_transaksi_kliring' => "14", 'nama_transaksi_kliring' => "Tolakan Masuk", 'id_jenis_transaksi' => 2, 'keterangan' => 2, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}

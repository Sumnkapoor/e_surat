<?php

use Illuminate\Database\Seeder;

class m_pekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_pekerjaan')->delete();
		  DB::table('m_pekerjaan')->insert([
		 [
            'id' => 1,
            'nama' => 'Akuntan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
			
        ],
		[
            'id' => 2,
            'nama' => 'Pengurus Partai Politik',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'nama' => 'Pensiunan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'nama' => 'Pejabat Negara',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'nama' => 'PNS',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 6,
            'nama' => 'Pengacara',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 7,
            'nama' => 'TNI/Polri',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 8,
            'nama' => 'Ibu Rumah Tangga',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 9,
            'nama' => 'Wiraswasta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 10,
            'nama' => 'Notaris',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 11,
            'nama' => 'Pelajar/Mahasiswa',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 12,
            'nama' => 'Profesional & Konsultan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 13,
            'nama' => 'Pegawai Swasta',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 14,
            'nama' => 'Pengajar dan Dosen',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 15,
            'nama' => 'Pedagang',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 16,
            'nama' => 'Pengrajin',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 17,
            'nama' => 'Petani & Nelayan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 18,
            'nama' => 'Buruh, Pembantu Rumah Tangga',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 19,
            'nama' => 'Pengurus & Pegawai Yayasan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 20,
            'nama' => 'Pengurus/Pegawai LSM',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 21,
            'nama' => 'Ulama/Pendeta/Pimp Org Keagamaan',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 22,
            'nama' => 'Pegawai BI/BUMN/BUMD',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 23,
            'nama' => 'Lain-Lain',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
        ]
		]);
    }
}

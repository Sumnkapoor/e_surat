<?php

use Illuminate\Database\Seeder;

class m_sifatKreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sifat_kredit')->delete();
		  DB::table('m_sifat_kredit')->insert([
		    [ 'id' => 1, 'kode' => "1", 'nama' => "Pembiyaan Bersama", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "2", 'nama' => "Channeling", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "3", 'nama' => "Kepada Debitur YBS", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "6", 'nama' => "Perjanjian Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "9", 'nama' => "Tanpa Perjanjian", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
		]);
    }
}

<?php

use Illuminate\Database\Seeder;

class m_kelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_kelas')->delete();
        DB::table('m_kelas')->insert([
		[
            'id' => 1,
            'id_lembaga' => 1,
            'name' => 'Coba Coba',
			'tahunsemester' => '20212',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),			
        ],
        [
            'id' => 2,
            'id_lembaga' => 1,
            'name' => 'TOT',
			'tahunsemester' => '20212',
            'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),			
        ]		
		]);
    }
}

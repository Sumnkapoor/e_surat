<?php

use Illuminate\Database\Seeder;

class m_daftarKliringSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_daftar_kliring')->delete();
		DB::table('m_daftar_kliring')->insert([
            ['id' => 1, 'kode_kliring' => '1111110', 'nama_kliring'=>'BANK STIE PERBANAS', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 2, 'kode_kliring' => '1111111', 'nama_kliring'=>'BANK BTN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 3, 'kode_kliring' => '1111112', 'nama_kliring'=>'BANK BNI', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 4, 'kode_kliring' => '1111113', 'nama_kliring'=>'BANK MANDIRI', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 5, 'kode_kliring' => '1111114', 'nama_kliring'=>'BANK BRI', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 6, 'kode_kliring' => '1111115', 'nama_kliring'=>'BANK NIAGA TUNJUNGAN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 7, 'kode_kliring' => '1111116', 'nama_kliring'=>'BANK NIAGA DARMO', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 8, 'kode_kliring' => '1111117', 'nama_kliring'=>'BANK BCA VETERAN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 9, 'kode_kliring' => '1111118', 'nama_kliring'=>'BANK BCA DARMO', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 10, 'kode_kliring' => '1111119', 'nama_kliring'=>'BANK DANAMON', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 11, 'kode_kliring' => '1111121', 'nama_kliring'=>'BANK INTERNASIONAL INDONE', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 12, 'kode_kliring' => '1111122', 'nama_kliring'=>'BANK BAD', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 13, 'kode_kliring' => '1111123', 'nama_kliring'=>'BANK PERMATA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 14, 'kode_kliring' => '1111124', 'nama_kliring'=>'BANK BUANA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 15, 'kode_kliring' => '1111125', 'nama_kliring'=>'BANK KESAWAN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 16, 'kode_kliring' => '1111126', 'nama_kliring'=>'BANK PRIMA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 17, 'kode_kliring' => '1111127', 'nama_kliring'=>'BANK HARFA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 18, 'kode_kliring' => '1111128', 'nama_kliring'=>'BANK AKITA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 19, 'kode_kliring' => '1111129', 'nama_kliring'=>'BANK YUDHA BHAKTI', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 20, 'kode_kliring' => '1111130', 'nama_kliring'=>'BANK SWADESI', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 21, 'kode_kliring' => '1111131', 'nama_kliring'=>'BANK MASPION', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 22, 'kode_kliring' => '1111132', 'nama_kliring'=>'BANK LIPPO', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 23, 'kode_kliring' => '1111133', 'nama_kliring'=>'BANK MUAMALAT', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 24, 'kode_kliring' => '1111134', 'nama_kliring'=>'BANK PANIN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 25, 'kode_kliring' => '1111135', 'nama_kliring'=>'BANK TUGU', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 26, 'kode_kliring' => '1111136', 'nama_kliring'=>'BANK CNB', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 27, 'kode_kliring' => '1111137', 'nama_kliring'=>'BANK INA', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 28, 'kode_kliring' => '1111138', 'nama_kliring'=>'BANK HSBC', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 29, 'kode_kliring' => '1111139', 'nama_kliring'=>'BANK BTPN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 30, 'kode_kliring' => '1111140', 'nama_kliring'=>'BANK BUKOPIN', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],

        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class m_nilaiTukarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	 
    public function run()
    {
	
		DB::table('m_nilai_tukar')->delete();
        DB::table('m_nilai_tukar')->insert([
            [ 'id' => 1, 'kurs_nama' => "Kurs TT", 'kurs_beli' => 9400, 'kurs_jual' => 9500, 'id_kelas' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 2, 'kurs_nama' => "Kurs BN", 'kurs_beli' => 10400, 'kurs_jual' => 10500, 'id_kelas' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            [ 'id' => 3, 'kurs_nama' => "Kurs TC", 'kurs_beli' => 8400, 'kurs_jual' => 8500, 'id_kelas' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
		]);
    }
}

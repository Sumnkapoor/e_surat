<?php

use Illuminate\Database\Seeder;

class m_sumberDanaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sumber_dana')->delete();
		DB::table('m_sumber_dana')->insert([
            ['id' => 1, 'kode' => "1", 'nama' => 'Hasil Usaha', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 2, 'kode' => "2", 'nama' => 'Warisan', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 3, 'kode' => "3", 'nama' => 'Hibah', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 4, 'kode' => "4", 'nama' => 'Hadiah', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 5, 'kode' => "5", 'nama' => 'Pinjaman', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 6, 'kode' => "6", 'nama' => 'Lainnya', 'status' => '1', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 7, 'kode' => "10", 'nama' => 'Gaji/Honor', 'status' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 8, 'kode' => "21", 'nama' => 'Usaha - Subsidi', 'status' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 9, 'kode' => "22", 'nama' => 'Usaha - Non Subsidi', 'status' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 10, 'kode' => "31", 'nama' => 'Lainnya - Subsidi', 'status' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
            ['id' => 11, 'kode' => "32", 'nama' => 'Lainnya - Non Subsidi', 'status' => '2', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),]
		]);
    }
}

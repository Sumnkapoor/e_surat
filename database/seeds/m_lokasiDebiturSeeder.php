<?php

use Illuminate\Database\Seeder;

class m_lokasiDebiturSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('m_lokasi_debitur')->delete();
		  DB::table('m_lokasi_debitur')->insert([
		    [ 'id' => 1, 'kode' => "1200", 'nama' => "Kepala Daerah Provinsi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'kode' => "1201", 'nama' => "Kab. Gresik", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'kode' => "1202", 'nama' => "Kab. Sidoarjo", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'kode' => "1203", 'nama' => "Kab. Mojokerto", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'kode' => "1204", 'nama' => "Kab. Jombang", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 6, 'kode' => "1205", 'nama' => "Kab. Sampang", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 7, 'kode' => "1206", 'nama' => "Kab. Pamekasan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 8, 'kode' => "1207", 'nama' => "Kab. Sumenep", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 9, 'kode' => "1208", 'nama' => "Kab. Bangkalan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 10, 'kode' => "1209", 'nama' => "Kab. Bondowoso", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 11, 'kode' => "1211", 'nama' => "Kab. Banyuwangi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 12, 'kode' => "1212", 'nama' => "Kab. Jember", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 13, 'kode' => "1213", 'nama' => "Kab. Malang", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 14, 'kode' => "1214", 'nama' => "Kab. Pasuruan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 15, 'kode' => "1215", 'nama' => "Kab. Probolinggo", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 16, 'kode' => "1216", 'nama' => "Kab. Lumajang", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 17, 'kode' => "1217", 'nama' => "Kab. Kediri", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 18, 'kode' => "1218", 'nama' => "Kab. Nganjuk", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 19, 'kode' => "1219", 'nama' => "Kab. Tulungagung", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 20, 'kode' => "1220", 'nama' => "Kab. Trenggalek", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 21, 'kode' => "1221", 'nama' => "Kab. Blitar", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 22, 'kode' => "1222", 'nama' => "Kab. Madiun", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 23, 'kode' => "1223", 'nama' => "Kab. Ngawi", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 24, 'kode' => "1224", 'nama' => "Kab. Magetan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 25, 'kode' => "1225", 'nama' => "Kab. Ponorogo", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 26, 'kode' => "1226", 'nama' => "Kab. Pacitan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 27, 'kode' => "1227", 'nama' => "Kab. Bojonegoro", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 28, 'kode' => "1228", 'nama' => "Kab. Tuban", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 29, 'kode' => "1229", 'nama' => "Kab. Lamongan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 30, 'kode' => "1230", 'nama' => "Kab. Situbondo", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 31, 'kode' => "1271", 'nama' => "Kota. Batu", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 32, 'kode' => "1288", 'nama' => "Kab./Kota Lainnya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 33, 'kode' => "1291", 'nama' => "Kota. Surabaya", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 34, 'kode' => "1292", 'nama' => "Kota. Mojokerto", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 35, 'kode' => "1293", 'nama' => "Kota. Malang", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 36, 'kode' => "1294", 'nama' => "Kota. Pasuruan", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 37, 'kode' => "1295", 'nama' => "Kota. Probolinggo", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 38, 'kode' => "1296", 'nama' => "Kota. Blitar", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 39, 'kode' => "1297", 'nama' => "Kota. Kediri", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 40, 'kode' => "1298", 'nama' => "Kota. Madiun", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        
        [ 'id' => 41, 'kode' => "1299", 'nama' => "Kota. Jember", 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],        

		]);
    }
}

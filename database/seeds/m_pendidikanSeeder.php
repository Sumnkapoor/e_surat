<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Pendidikan;

class m_pendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	 

    public function run()
    {
	
		DB::table('m_pendidikan')->delete();
        DB::table('m_pendidikan')->insert([
		 [
            'id' => 1,
            'name' => 'SD',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 2,
            'name' => 'SMP',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 3,
            'name' => 'SMA',
           'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 4,
            'name' => 'D1',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 5,
            'name' => 'D2',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 6,
            'name' => 'D3',
           'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 7,
            'name' => 'S1',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 8,
            'name' => 'S2',
            'created_at' => date("Y-m-d H:i:s"),
        ],
		[
            'id' => 9,
            'name' => 'S3',
            'created_at' => date("Y-m-d H:i:s"),
        ]
		]);
    }
}

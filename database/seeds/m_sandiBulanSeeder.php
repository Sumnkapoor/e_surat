<?php

use Illuminate\Database\Seeder;

class m_sandiBulanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_sandi_bulan')->delete();
		  DB::table('m_sandi_bulan')->insert([
		    [ 'id' => 1, 'bulan' => "01", 'sandi' => 25, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 2, 'bulan' => "02", 'sandi' => 50, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 3, 'bulan' => "03", 'sandi' => 75, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 4, 'bulan' => "04", 'sandi' => 100, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 5, 'bulan' => "05", 'sandi' => 125, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 6, 'bulan' => "06", 'sandi' => 150, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 7, 'bulan' => "07", 'sandi' => 175, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 8, 'bulan' => "08", 'sandi' => 200, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 9, 'bulan' => "09", 'sandi' => 225, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 10, 'bulan' => "10", 'sandi' => 250, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 11, 'bulan' => "11", 'sandi' => 275, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],
        [ 'id' => 12, 'bulan' => "12", 'sandi' => 300, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),],				        
		]);
    }
}

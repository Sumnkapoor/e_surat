<?php

use Illuminate\Database\Seeder;

class m_jenisrekeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('m_jenis_rekening')->delete();
		  DB::table('m_jenis_rekening')->insert([
		 [
            'jenis_id' => 1,
            'jenis_rekening' => 'Giro',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'jenis_id' => 2,
            'jenis_rekening' => 'Tabungan',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'jenis_id' => 3,
            'jenis_rekening' => 'Deposito',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],
		[
            'jenis_id' => 4,
            'jenis_rekening' => 'Pinjaman',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ],[
            'jenis_id' => 5,
            'jenis_rekening' => 'Sertifikat Deposito',
            'dt_record' => date("Y-m-d H:i:s"),
			'user_record' => "Administrator",
			'dt_modified' => date("Y-m-d H:i:s"),
			'user_modified' => "Administrator",
        ]
		
		]);
    }
	}

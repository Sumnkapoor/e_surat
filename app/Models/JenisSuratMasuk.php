<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisSuratMasuk extends Model
{
    protected $table = 'kesekretariatan.ms_jenissuratmasuk';

    protected $primaryKey = 'jenissuratmasukid';

    public $timestamps = false;
    protected $fillable = [
        'jenissuratmasuknama'		
    ];	   
    
}

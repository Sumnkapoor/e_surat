<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratKeluarKirim extends Model
{
    protected $table = 'kesekretariatan.tr_seksuratkeluarkirim';

    protected $primaryKey = 'suratkeluarkirimid';

    public $timestamps = false;
    protected $fillable = [        
        'idsuratkeluar',		
        'idpegawai',        
    ];	   
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratKeluarTeruskan extends Model
{
    protected $table = 'kesekretariatan.tr_seksuratkeluarteruskan';

    protected $primaryKey = 'suratkeluarteruskanid';

    public $timestamps = false;
    protected $fillable = [        
        'idsuratkeluar',		
        'kodeunit',        
    ];	   
    
}

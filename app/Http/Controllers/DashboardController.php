<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index()
    {	
                
        $kodeunit = Session::get('kodeunit');
        $status = Session::get('admin');
        
        if($status=='y') {

            // Cari Surat Masuk
            $LdataSuratMasuk = DB::table('kesekretariatan.tr_seksuratmasuk as a')                  
            ->rightJoin('kesekretariatan.tr_seksuratdisposisi as d', 'a.suratmasukid', '=', 'd.idsuratmasuk')            
            ->select('a.suratmasukid')  
            ->whereNotNull('d.suratdisposisinosurat')              
            ->count();

            // Cari Surat Keluar
            $LdataSuratKeluar = DB::table('kesekretariatan.tr_seksuratkeluar')                              
            ->select('suratkeluarid')              
            ->count();
            
            // Cari Surat Masuk Belum Disposisi
            $LdataBlmDisposisi = DB::table('kesekretariatan.tr_seksuratmasuk as a')                  
            ->leftJoin('kesekretariatan.tr_seksuratdisposisi as d', 'a.suratmasukid', '=', 'd.idsuratmasuk')            
            ->select('suratmasukid')  
            ->whereNull('suratdisposisinosurat')              
            ->count();

        } else {

            // Cari Surat Masuk
            $LdataSuratMasuk = DB::table('kesekretariatan.tr_seksuratmasuk as a')                
            ->rightJoin('kesekretariatan.tr_seksuratdisposisi as d', 'a.suratmasukid', '=', 'd.idsuratmasuk')            
            ->select('suratmasukid')  
            ->whereNotNull('suratdisposisinosurat')  
            ->where(function ($query) use ($kodeunit) {
                return $query->where('a.kodeunitpenerima', '=', $kodeunit)
                      ->orWhere('d.kodeunitditeruskan', '=', $kodeunit);
            })                                 
            ->count();

            // Cari Surat Keluar
            $LdataSuratKeluar = DB::table('kesekretariatan.tr_seksuratkeluar')                            
            ->select('suratkeluarid')              
            ->where('kodeunitpembuat', '=', $kodeunit)            
            ->count();
        }
                
        // if($LdataSuratMasuk) {
        //     return response()->json([
        //         'status'=>'oke',
        //         'data' => $LdataSuratMasuk
        //         ]);
        // } else {
        //     return response()->json(['status'=>'failed']);
        // }
                
        return view('frontend/page/dashboard/index', compact('LdataSuratMasuk','LdataSuratKeluar','LdataBlmDisposisi')); 
    }                   
    

    private function validateRequest($request, $id=0){

        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',
            'min' => 'Panjang minimal <b>:attribute</b> huruf.',
            'numeric' => 'Inputan harus angka.',
            'unique' => 'Data <b>:attribute</b> ":input" sudah ada, tidak boleh sama.',
        ];

        return Validator::make($request->all(), [
//            "nomor_rekening" => "required|unique:t_rekening_nasabah,nomor_rekening".($id ? ",".$id.",id" : "" ),            
        ], $messages);
    }    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\JenisSuratMasuk;
use App\Models\SuratMasuk;
use App\Models\Disposisi;
use App\Models\MasterDisposisi;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Session;

class lapSuratMasukController extends Controller
{

    // use AuthenticatesUsers;
    protected $redirectTo = '/';

	public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {		
        $data = array(
            'head' => 'LAPORAN',
            'title' => 'LAPORAN SURAT MASUK',            
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
        );        
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('lap_surat_masuk/index',compact('data'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }
        
    public function edit(Request $request)
    {		
        $data = array(
            'head' => 'LAPORAN SURAT MASUK',
            'title' => 'DETAIL SURAT MASUK',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'act' => 'PUT',
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );        

        $LjenisSuratMasuk = JenisSuratMasuk::get();                
        $LsuratMasuk = SuratMasuk::where('suratmasukid','=',$request->id)->get();
        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);
        //return view('edit_perkiraan/index', compact('data'));
        $returnHTML = view('lap_surat_masuk/add',compact('data','LjenisSuratMasuk','LdataDepartement','LsuratMasuk'))->render();
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function disposisi(Request $request)
    {		

        // Cari Nomer Disposisi
        $orderObj = DB::table('kesekretariatan.tr_seksuratdisposisi')
                        ->select('suratdisposisinomoragenda')                        
                        ->latest('suratdisposisiid')->first();   

        if ($orderObj) {
            $lastKodeNumber = $orderObj->suratdisposisinomoragenda;            
            $KodeNumber2 = str_pad($lastKodeNumber + 1, 4, "0", STR_PAD_LEFT);
            
        } else {
            $KodeNumber2 = str_pad(1, 4, "0", STR_PAD_LEFT);                 
        }

        $data = array(
            'head' => 'LAPORAN SURAT MASUK',
            'title' => 'LEMBAR DISPOSISI',
            'subtitle' => Session::get('subtitle'),
            'alamatKampus' => Session::get('alamat'),
            'btnClass' => 'btn btn-primary btn-sm px-4',
            'btnClassDisposisi' => 'btn btn-outline-primary btn-detail',
            'btnAdd' => 'Tambah',
            'id' => $request->id,
            'noDisposisi' => $KodeNumber2,
            'classFormSelect' => 'form-select form-select-sm',
            'classFormControl' => 'form-control form-control-sm',
            'classFormSelect2' => 'single-select',
        );    

        $LdataDepartement = DB::select (
			DB::raw('
                select * from gate.ms_unit
                where level is not null
                order by level, kodeunitparent  
				
			')
		);

        $LdataPegawai = DB::table('akademik.ms_pegawai')                
            ->select('nama','nik','idpegawai')     
            ->where('statuspeg','=','Aktif')
            ->orderBy('nama','asc')
            ->get();

        $LMasterDisposisi = MasterDisposisi::get();

        if(($request->id) > 0){
            $dataDisposisi = DB::select (
                DB::raw('
                    select *
                    from kesekretariatan.tr_seksuratdisposisi 
                    where idsuratmasuk = '.$request->id.'
                ')
            );
            $returnHTML = view('lap_surat_masuk/disposisi',compact('data','LMasterDisposisi','LdataDepartement','LdataPegawai','dataDisposisi'))->render();
        } else {
            $returnHTML = view('lap_surat_masuk/disposisi',compact('data','LdataDepartement','LdataPegawai','LMasterDisposisi'))->render();
        }

        //return view('edit_perkiraan/index', compact('data'));        
        return response()->json( array('success' => true, 'html'=>$returnHTML) );        
    }

    public function getData(Request $request)
    {
        $awal = date('n/d/Y', strtotime($request->awal));
        $akhir = date('n/d/Y', strtotime($request->akhir));
        $Ldata = SuratMasuk::get();         
        $kodeunit = Session::get('kodeunit');

        $Ldata = DB::table('kesekretariatan.tr_seksuratmasuk as a')
            ->leftJoin('kesekretariatan.ms_jenissuratmasuk as b', 'a.idjenissuratmasuk', '=', 'b.jenissuratmasukid')
            ->leftJoin('gate.ms_unit as c', 'a.kodeunitpenerima', '=', 'c.kodeunit')            
            ->rightJoin('kesekretariatan.tr_seksuratdisposisi as d', 'a.suratmasukid', '=', 'd.idsuratmasuk')            
            ->select('a.*','b.*','c.kodeunit','c.namaunit','d.suratdisposisinosurat')     
            ->where(function ($query) use ($kodeunit) {
                return $query->where('a.kodeunitpenerima', '=', $kodeunit)
                      ->orWhere('d.kodeunitditeruskan', '=', $kodeunit);
            })
            ->whereNotNull('suratdisposisinosurat')            
            ->whereBetween('suratmasuktanggal', [$awal,$akhir])
            ->orderBy('suratmasuktanggal','asc')
            ->get();
        
        if($Ldata) {
            return response()->json([
                'status'=>'oke',
                'data' => $Ldata
                ]);
        } else {
            return response()->json(['status'=>'failed']);
        }

    }
            
}

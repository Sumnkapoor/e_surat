<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group( ['middleware' => 'auth' ], function()
{    
  
    Route::get('/', 'DashboardController@index');

    // Setup Disposisi
    Route::get('/masterDisposisi','MasterDisposisiController@index');    
    Route::get('/getDataJson/masterDisposisi','MasterDisposisiController@getData');    
    Route::put('/masterDisposisi/{id}','MasterDisposisiController@update'); 
    Route::get('/delete/masterDisposisi/{id}','MasterDisposisiController@destroy');
    Route::post('/masterDisposisi','MasterDisposisiController@store');

    // Setup Departement
    Route::get('/departement','DepartementController@index');    
    Route::get('/getDataJson/departement','DepartementController@getData');    
    Route::get('/addPic','DepartementController@pic');        
    Route::put('/departement/{id}','DepartementController@update'); 

    // Setup Jenis Surat
    Route::get('/jenisSurat','JenisSuratController@index');    
    Route::get('/getDataJson/jenisSurat','JenisSuratController@getData');    
    Route::post('/jenisSurat','JenisSuratController@store');
    Route::post('/detailSurat','JenisSuratController@store2');
    Route::post('/cariDetailSurat','JenisSuratController@searchDetailSurat');
    Route::put('/jenisSurat/{id}','JenisSuratController@update'); 
    Route::put('/detailSurat/{id}','JenisSuratController@update2'); 
    Route::get('/delete/jenisSurat/{id}','JenisSuratController@destroy');

    // Setup Jenis Surat Masuk
    Route::get('/jenisSuratMasuk','JenisSuratMasukController@index');    
    Route::get('/getDataJson/jenisSuratMasuk','JenisSuratMasukController@getData');    
    Route::get('/delete/jenisSuratMasuk/{id}','JenisSuratMasukController@destroy');
    Route::post('/jenisSuratMasuk','JenisSuratMasukController@store');
    Route::put('/jenisSuratMasuk/{id}','JenisSuratMasukController@update'); 

    // Proses Surat Keluar
    Route::get('/suratKeluar','suratKeluarController@index');       
    Route::post('/getDataJson/suratKeluar','suratKeluarController@getData');     
    Route::post('/getDataJson/suratKeluarSend','suratKeluarController@getDataSend');     
    Route::post('/getDataJson/suratKeluarTeruskan','suratKeluarController@getDataTeruskan');     
    Route::get('/addSuratKeluar','suratKeluarController@add');          
    Route::post('/editSuratKeluar','suratKeluarController@edit');              
    Route::post('/kirimSuratKeluar','suratKeluarController@send');          
    Route::post('/forwardSuratKeluar','suratKeluarController@forward');    
    Route::get('/delete/suratKeluar/{id}','suratKeluarController@destroy');          
    Route::get('/delete/suratKeluarSend/{id}','suratKeluarController@destroySend');
    Route::get('/delete/suratKeluarTeruskan/{id}','suratKeluarController@destroyTeruskan');
    Route::post('/saveSendSurat','suratKeluarController@storeSend');   
    Route::post('/saveForwardSurat','suratKeluarController@storeTeruskan');   
    Route::post('/saveHeaderSuratKeluar','suratKeluarController@store');   
    Route::post('/saveContentSuratKeluar','suratKeluarController@updateContent');       

    // Proses Surat Masuk
    Route::get('/suratMasuk','suratMasukController@index');        
    Route::post('/getDataJson/suratMasuk','suratMasukController@getData');    
    Route::get('/addSuratMasuk','suratMasukController@add');            
    Route::post('/addDisposisi','suratMasukController@disposisi');   
    Route::post('/disposisi','suratMasukController@store2');   
    Route::put('/disposisi/{id}','suratMasukController@update2'); 
    Route::post('/editSuratMasuk','suratMasukController@edit');              
    Route::post('/saveSuratMasuk','suratMasukController@store');   
    Route::put('/saveSuratMasuk/{id}','suratMasukController@update'); 
    Route::get('/delete/suratMasuk/{id}','suratMasukController@destroy');    

     // Laporan Surat Masuk
     Route::get('/lapSuratMasuk','lapSuratMasukController@index');        
     Route::post('/getDataJson/lapSuratMasuk','lapSuratMasukController@getData');    
     Route::post('/addLapDisposisi','lapSuratMasukController@disposisi');        
     Route::post('/editLapSuratMasuk','lapSuratMasukController@edit');  
     
     // Dashboard
     //Route::post('/dashboard','DashboardController@index');   
	
});

  // Login
	Route::get('/logout', 'LoginController@logout');
  Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/getDataLogin', 'LoginController@login');
	Route::post('/login','LoginController@login');
	
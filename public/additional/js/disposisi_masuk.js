var data_table;

$(document).ready(function () {
    
    CKEDITOR.replace( 'perihal' );
    $('button#btn_simpan').on('click', function () {        
        insertUpdateProses();
    });    

    $('button#btn_kembali').on('click', function () {        
        back();
    });    
        
    $('.single-select').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.single-select2').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true
        }),
    
    $('.timepicker').pickatime()

    
    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });
    
});
    
    function back() {
        window.history.go(-1);return false;
    }

    function insertUpdateProses() {

        var form = $('#formEntry');
        if (form.valid() == true) {
            var perihal = CKEDITOR.instances['perihal'].getData();
            var dataForm = form.serialize();
            dataForm += '&perihal='+perihal;

            var method = $('#idTr').val();
            var action_url = "" + base_url + "/disposisi";            
            var action_type = "Tambah";
            var type = "POST";
            if (method > 0) {
                action_url = "" + base_url + "/disposisi/" + $('#idTr').val();
                action_type = "Ubah";
                type = "PUT";
            }

            $.ajax({
                type: type,
                url: action_url,
                dataType: 'JSON',
                data: dataForm,                
                beforeSend: function(){
                    BeforeSend();
                },
                complete: function(){
                    AfterSend();
                },
                success: function (data) {
                    if (data.status == 'insert_successful') {
                        success_noti('Berhasil ' + action_type + ' Data');
                        $('.modal-form').modal('toggle');
                        data_table.ajax.reload(null, false);
                    } else if (data.status == 'insert_failed') {
                        error_noti('Gagal ' + action_type + ' Data'); 

                        var errors = data.error;
                        errorValidationLaravel(errors, '#error-validation');

                    } else {
                        error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
                    }
                },

                error: function (xmlhttprequest, textstatus, message) {
                    error_noti('Koneksi Ke Server Gagal, '+message);
                }

            });            
        
        } else {                        
            error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        }
    }



    // var validator = $('#form').validate({

    //     rules: {
    //         kode_perkiraan: {required: true},
    //         nama_perkiraan: {required: true},			
    //     },

    //     highlight: function (element, errorClass, validClass, error) {
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
    //     },

    //     unhighlight: function (element, errorClass, validClass) {
    //         $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
    //         $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
    //     }
    // });

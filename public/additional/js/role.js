var data_table;
$(document).ready(function () {
    loadData();

    $('button#tambah').on('click', function () {
        clearModal(); 
        $('#modal_label').text('Form Tambah Role');
        $('#method_field').val("POST");
        $(".modal-form").modal('show');
    });

    $('i#btn_simpan').on('click', function () {
        insertUpdateProses();
    });
});

function loadData() {
        data_table = $('#role_datatable').DataTable({
        processing: true,
        ajax: {
            "url": "" + base_url + '/getDataJson/role',
            'type': 'GET',
            'dataType': 'JSON',
            'error': function (xhr, textStatus, ThrownException) {
                alert('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
            }
        },
        columns: [{
            title: "ID ",
            data: "role_id",
            visible: false,
            sortable: true,
            class: "text-center"
        }, {
            title: "Moduls",
            data: "moduls",
            visible: false,
            sortable: true,
            class: "text-center"
        }, {
            title: "Nama Role",
            data: "nama_role",
            visible: true,
            sortable: true,
            class: ""
        },  {
            title: "Hak Akses",
            data: "role_id",
            visible: true,
            sortable: true,
            class: "",
            render: function (data, type, full, meta) {
                var result = '<ul>';
                var moduls = full.moduls;
                for (var key in moduls) {
                    if (moduls.hasOwnProperty(key)) { 
                        result += "<li>"+moduls[key].nama_modul+"</li>";
                    }
                }
                result += '</ul>';
                return result;
            }
        }, {
            title: "Tanggal Input",
            data: "dt_record",
            visible: true,
            sortable: true,
            class: "",
            render: function (data, type, full, meta) {
                   if(data!=null) {
                    return '<td class="text-center">'+timeIndoFormat(data)+'</td>';
                    } else {
                    	return "-";
                    }
            }
        }, {
            title: "Aksi",
            data: "id",
            visible: true,
            sortable: false,
            class: "text-center",
            render: function (data, type, full, meta) {
                var result = '';
                result += '<td class="text-center">';
                result +='<button class="btn btn-warning btn-sm btn-edit"> <i class="bx bx-edit"></i> </button>&nbsp;';
                result += '<button class="btn btn-danger  btn-sm btn-delete"> <i class="bx bx-trash"></i></button>';
                result += '</td>';
                return result;
            }
        }],
        "drawCallback": function (settings) {
            $('.btn-edit').on('click', function () {
                clearModal(); 

                var data = data_table.row($(this).parents('tr')).data();
                var moduls =data.moduls;

                for(var i = 0; i < moduls.length; i++) {                    
                    $('#checkboxModul'+moduls[i].modul_id).prop('checked', true);
                }
                // .prop('checked', true);

                $('#role_id').val(data.role_id);
                $('#nama_role').val(data.nama_role);

                $('#modal_label').text('Form Ubah Role');
                $('#method_field').val("PUT");
                $(".modal-form").modal('show');
            });
            $('.btn-delete').on('click', function () {
                var data = data_table.row($(this).parents('tr')).data();

                Swal.fire({
                    title: 'Yakin hapus "' + data.nama_role + '"?',
                    text: "Setelah terhapus data tidak bisa kembali!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        ajaxDeleteData("role", data.role_id, data_table );
                    }
                })
            });
        }
    });
} 

function insertUpdateProses() {
    var form = $('#form_role');
    if (form.valid() == true) {

        var method = $('#method_field').val();
        var action_url = "" + base_url + "/role";
        var action_type = "Tambah";
        if (method === "PUT") {
            action_url = "" + base_url + "/role/" + $('#role_id').val();
            action_type = "Ubah";
        }
        $.ajax({
            type: 'POST',
            url: action_url,
            dataType: 'JSON',
            data: form.serialize(),
            beforeSend: function () {
                sweetAlertLoading('Memproses');
            },
            success: function (data) {
                if (data.status == 'insert_successful') {
                    sweetAlertDefault('<b>Berhasil ' + action_type + ' Data</b>', 'success', 2000 );

                    $('.modal-form').modal('toggle');
                    data_table.ajax.reload(null, false);
                } else if (data.status == 'insert_failed') {                    
                    sweetAlertDefault('<b>Gagal ' + action_type + 'sss </b>', 'error', 2000 ); 
                    
                    var errors = data.error;
                    errorValidationLaravel(errors, '#error-validation');
                    
                } else {
                    sweetAlertDefault('<b>Gagal ' + action_type + ' (Kesalahan Sistem) </b>', 'error', 2000 );  
                }
            },
            error: function (xmlhttprequest, textstatus, message) {
                sweetAlertDefault('<b>Koneksi Ke Server Gagal, Mohon Refresh Halaman</b>', 'error', 2000 ); 
            }
        });

    } else {
        sweetAlertLoading('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah',1000);
    }
}

var validator = $('#form_role').validate({
    rules: {
        nama_role: {
            required: true
        },
        'modul[]': {
            required: true
        }
    },
    messages: {
        'modul[]': {
            required: '<i class="fa fa-pencil" style="color:red;"></i> Minimal Pilih 1 Modul'
        }
    },
    highlight: function (element, errorClass, validClass, error) {
        $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
        $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
        // $().find("[id=" + element.id + "]").removeClass('is-valid');
        
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
        $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
    }
});

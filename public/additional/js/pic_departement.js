var data_table;

$(document).ready(function () {
    
    //loadData();
    $('.single-select').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.single-select2').select2({
        theme: 'bootstrap4',		
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
        allowClear: Boolean($(this).data('allow-clear')),
    });
    
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true
        }),
    
    $('.timepicker').pickatime()    
    
    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });
    
    $('button#tambah').on('click', function () {        
         clearModal();         
         $('#modal_label').text('Form Tambah Data');
         $('#method_field').val("POST");
         $(".modal-form").modal('show');
    });

    $('button#btn_simpan').on('click', function () {        
        insertUpdateProses();
    });    
        
});

    function loadData() {

        data_table  = $('#example2').DataTable({            
            processing: false,
            lengthMenu: [[50, 100, 200, -1], [50, 100, 200, "All"]],
            initComplete: function() {
                data_table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
                $("#example2").show();
            },
            buttons: ['excel', 'pdf'],
            ajax: {
                "url": "" + base_url + '/getDataJson/jenis_surat',
                'type': 'GET',
                'dataType': 'JSON',
                'error': function (xhr, textStatus, ThrownException) {                    
                    error_noti('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
                }
            },

            columns: [
            {
                title: "Aksi",
                data: "biaya_id",
                width: "15%",
                visible: true,
                sortable: false,
                class: "text-center",
                render: function (data, type, full, meta) {
                    var result = '';
                    result += '<td class="text-center">';
                    result +=
                        '<button class="btn btn-warning btn-sm btn-edit"> <i class="bx bx-edit"></i> </button>&nbsp;';
                    result +=
                        '<button class="btn btn-danger  btn-sm btn-delete"> <i class="bx bx-trash"></i> </button>';
                    result += '</td>';
                    return result;
                }
            },{
                title: "Nama Surat ",
                data: "biaya_kode",
                width: "20%",
                visible: true,
                sortable: true,
                class: "text-center"
            }, {
                title: "Kode Surat",
                data: "biaya_nama",
                width: "40%",
                visible: true,
                sortable: true,
                class: ""
            }],

            "drawCallback": function (settings) {
                // $('.btn-edit').on('click', function () {
                //     clearModal();
                //     var data = data_table.row($(this).parents('tr')).data();
                //     $('#id').val(data.id);
                //     $('#kode_perkiraan').val(data.kode_perkiraan);
				// 	$('#nama_perkiraan').val(data.nama_perkiraan);					
                //     $('#keterangan').val(data.df_trans_perkiraan);					

                //     $('#modal_label').text('Form Ubah');
                //     $('#method_field').val("PUT");
                //     $(".modal-form").modal('show');
                // });

                // $('.btn-delete').on('click', function () {
                //     var data = data_table.row($(this).parents('tr')).data();
                //     Lobibox.confirm({
                //         iconClass: true,
                //         title: 'Delete Data',                        
                //         msg: 'Yakin Hapus Data "' + data.nama_perkiraan + '"?',
                //         callback: function ($this, type, ev) {
                //             if(type=='yes'){
                //                 deleteProses(data.id);
                //             }        
                //         }
                //     });                    
                // });
            }
		});            
    }

    function deleteProses(id) {
        // $.ajax({
        //     type: 'GET',
        //     url: "" + base_url + "/delete/edit_perkiraan/" + id,
        //     dataType: 'JSON',            

        //     success: function (data) {
	    //         if (data.status == 'delete_successful') {
	    //             success_noti('Data Berhasil Terhapus');
	    //             data_table.ajax.reload(null, false);
	    //         } else if (data.status == 'delete_failed') {
	    //             error_noti('Data Gagal Dihapus');
	    //         } else {
        //             error_noti('Data Gagal Dihapus (Kesalahan Sistem)');
        //         }
        //     },

        //     error: function (xmlhttprequest, textstatus, message) {
        //         error_noti('Koneksi Ke Server Gagal, Mohon Refresh Halaman')
        //     }
        // });
	}

    function insertUpdateProses() {

        // var form = $('#form_edit_perkiraan');
        // if (form.valid() == true) {
            
        //     var method = $('#method_field').val();
        //     var action_url = "" + base_url + "/edit_perkiraan";            
        //     var action_type = "Tambah";
        //     if (method === "PUT") {
        //         action_url = "" + base_url + "/edit_perkiraan/" + $('#id').val();
        //         action_type = "Ubah";
        //     }

        //     $.ajax({
        //         type: 'POST',
        //         url: action_url,
        //         dataType: 'JSON',
        //         data: form.serialize(),                

        //         success: function (data) {
        //             if (data.status == 'insert_successful') {
        //                 success_noti('Berhasil ' + action_type + ' Data');
        //                 $('.modal-form').modal('toggle');
        //                 data_table.ajax.reload(null, false);
        //             } else if (data.status == 'insert_failed') {
        //                 error_noti('Gagal ' + action_type + ' Data'); 

        //                 var errors = data.error;
        //                 errorValidationLaravel(errors, '#error-validation');


        //             } else {
        //                 error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
        //             }
        //         },

        //         error: function (xmlhttprequest, textstatus, message) {
        //             error_noti('Koneksi Ke Server Gagal, '+message);
        //         }

        //     });            
        
        // } else {                        
        //     error_noti('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah');
        // }
    }



    var validator = $('#form').validate({

        rules: {
            kode_perkiraan: {required: true},
            nama_perkiraan: {required: true},			
        },

        highlight: function (element, errorClass, validClass, error) {
            $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
        },

        unhighlight: function (element, errorClass, validClass) {
            $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
            $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
        }
    });

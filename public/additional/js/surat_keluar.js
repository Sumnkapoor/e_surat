var data_table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');		


$(document).ready(function () {
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var ddp = String(today.getDate()-7);
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    todayPrev = yyyy + '-' + mm + '-' + ddp;
    $('#tgl_awal').val(todayPrev);  
    $('#tgl_akhir').val(today);  

    loadData(todayPrev,today);      
    
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: true
    }),

    $(function () {
        $('#date-time').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm'
        });
        $('#date').bootstrapMaterialDatePicker({
            time: false
        });
        $('#time').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm'
        });
    });        
    
    $('button#btnCari').on('click', function () {        
        var awal = document.getElementById('tgl_awal').value;
        var akhir = document.getElementById('tgl_akhir').value;
        $('#example2').DataTable().destroy();
        loadData(awal,akhir);
    });

});
            
$('.actionx').click(function(event){			      			                  
    var urlx = $(this).attr('data-href');

    $.ajax({
        url: "" + base_url + urlx,
        type: 'get',
        cache: false,
        data: {
            _token: CSRF_TOKEN,                
        },	
        beforeSend: function(){
            BeforeSend();
        },
        complete: function(){
            AfterSend();
        },								
        success: function (data) {					
            $('.isiContent').html(data.html);					
        },

        error: function (xhr, status, error, xmlhttprequest, textstatus, message) {						
            alert(xmlhttprequest+"/"+textstatus+"/"+message);																						
        }
    });				
});			

function loadData(todayPrev,today) {        

    data_table  = $('#example2').DataTable({            
        processing: false,
        lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "All"]],            
        ajax: {
            url: "" + base_url + '/getDataJson/suratKeluar',
            type: 'post',
            cache: false,
            // dataType: JSON,
            beforeSend: function(){
                BeforeSend();
            },
            complete: function(){
                AfterSend();
            },
            data: {
                _token: CSRF_TOKEN,                
                awal: todayPrev,                
                akhir: today
            },
            error: function (xhr, textStatus, ThrownException) {                    
                error_noti('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
            }			                
        },


        columns: [
        {
            title: "Aksi",            
            width: "15%",
            visible: true,
            sortable: false,
            class: "text-center",
            render: function (data, type, row) {                                
                var jumlahKirim = (row.jumlahkirim==null) ? 0:row.jumlahkirim;
                var jumlahTeruskan = (row.jumlahteruskan==null) ? 0:row.jumlahteruskan;
                
                var result = '';

                    result += '<div class="btn-group">';
                    result += '<button type="button" class="btn btn-outline-primary btn-sm">Aksi</button>';
                    result += '<button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false"><span class="visually-hidden">Toggle Dropdown</span></button>';
                    result += '<ul class="dropdown-menu">';
                    result += '<li><a title="Ubah Data" class="dropdown-item btn-edit actionx" data-href="/editSuratKeluar">Edit</a></li>';
                    result += '<li><a title="Kirim Surat Ke" class="dropdown-item btn-send actionx" data-href="/kirimSuratKeluar">Kirim <span class="badge bg-primary">'+jumlahKirim+'</span></a></li>';
                    result += '<li><a title="Teruskan Surat Ke" class="dropdown-item btn-forward actionx" data-href="/forwardSuratKeluar">Teruskan <span class="badge bg-success">'+jumlahTeruskan+'</span></a></li>';
                    result += '<li><hr class="dropdown-divider"></li>';
                    result += '<li><a title="Hapus Data" class="dropdown-item btn-delete" href="#">Hapus</a></li>';
                    result += '</ul></div>';
                    
                // result += '<div class="btn-group" role="group" aria-label="First group">';
                // result += '<a title="Ubah Data" class="btn btn-outline-warning btn-sm btn-edit px-2 ms-2 actionx" data-href="/editSuratKeluar"><i class="bx bx-edit me-0"></i></a>';
                // result += '<a title="Kirim Ke" class="btn btn-outline-dark btn-sm btn-send px-2 ms-2 actionx" data-href="/kirimSuratKeluar"><i class="bx bx-send me-0"></i></a>';
                // result += '<a title="Teruskan Ke" class="btn btn-outline-primary btn-sm btn-forward px-2 ms-2 actionx" data-href="/teruskanSuratKeluar"><i class="bx bx-fast-forward-circle me-0"></i></a>';
                // result += '<button title="Hapus Data" class="btn btn-outline-danger btn-sm btn-delete px-2 ms-2"> <i class="bx bx-trash me-0"></i> </button>';
                // result += '</div>';
                return result;
            }
        },{
            title: "File",                
            width: "5%",
            visible: true,
            sortable: true,
            class: "text-center",
            render: function (data, type, row) {                
                var file = row.suratkeluarfilename;
                var result = '';                                        

                if(file === null){
                    result += '';    
                } else {                        
                    result += '<a title="Lihat File" href="'+base_url+'/upload/surat_keluar/'+file+'" target="_blank" class="btn btn-outline-success btn-sm btn-file px-2 ms-2" target="_blank"><i class="bx bx-file me-0"></i></a>';
                }
                                    
                return result;
            }
        },{
            title: "No. Surat",
            data: "suratkeluarnomor",
            width: "15%",
            visible: true,
            sortable: true,
            class: ""
        },{
            title: "Tanggal",
            data: "suratkeluartanggal",
            width: "10%",
            visible: true,
            sortable: true,
            class: ""
        },{
            title: "Departement",
            data: "namaunit",
            width: "25%",
            visible: true,
            sortable: true,
            class: ""
        },{
            title: "Perihal",
            data: "suratkeluarperihal",
            width: "25%",
            visible: true,
            sortable: true,
            class: ""
        },{
            title: "Jenis Surat",
            data: "jenissuratnama",
            width: "10%",
            visible: true,
            sortable: true,
            class: ""
        }],

        "drawCallback": function (settings) {
            $('.actionx').on('click', function () {
                           
                var data = data_table.row($(this).parents('tr')).data();
                var urlx = $(this).attr('data-href');                                    
    
                $.ajax({
                    url: "" + base_url + urlx,
                    type: 'post',
                    cache: false,
                    data: {
                        _token: CSRF_TOKEN,                
                        id: data.suratkeluarid
                    },	
                    beforeSend: function(){
                        BeforeSend();
                    },
                    complete: function(){
                        AfterSend();
                    },								
                    success: function (data) {					
                        $('.isiContent').html(data.html);					
                    },
            
                    error: function (xhr, status, error, xmlhttprequest, textstatus, message) {						
                        alert(xmlhttprequest+"/"+textstatus+"/"+message);																						
                    }
                });	
            });
            
            $('.btn-delete').on('click', function () {
                var data = data_table.row($(this).parents('tr')).data();
                Lobibox.confirm({
                    iconClass: true,
                    title: 'Delete Data',                        
                    msg: 'Yakin Hapus Data No. Surat"' + data.suratkeluarnomor + '"?',
                    callback: function ($this, type, ev) {
                        if(type=='yes'){
                            deleteProses(data.suratkeluarid);
                        }        
                    }
                });                    
            });
        }
    });            
}

function deleteProses(id) {
    $.ajax({
        type: 'GET',
        url: "" + base_url + "/delete/suratKeluar/" + id,
        dataType: 'JSON',            
        beforeSend: function(){
            BeforeSend();
        },
        complete: function(){
            AfterSend();
        },
        success: function (data) {
            if (data.status == 'delete_successful') {
                success_noti('Data Berhasil Terhapus');
                data_table.ajax.reload(null, false);
            } else if (data.status == 'delete_failed') {
                error_noti('Data Gagal Dihapus');
            } else {
                error_noti('Data Gagal Dihapus (Kesalahan Sistem)');
            }
        },

        error: function (xmlhttprequest, textstatus, message) {
            error_noti('Koneksi Ke Server Gagal, Mohon Refresh Halaman')
        }
    });
}

var validator = $('#form').validate({

    rules: {
        kode_perkiraan: {required: true},
        nama_perkiraan: {required: true},			
    },

    highlight: function (element, errorClass, validClass, error) {
        $(element.form).find("[id=" + element.id + "]").addClass('is-invalid');
        $(element.form).find("[id=" + element.id + "]").removeClass('is-valid');
    },

    unhighlight: function (element, errorClass, validClass) {
        $(element.form).find("[id=" + element.id + "]").removeClass('is-invalid');
        $(element.form).find("[id=" + element.id + "]").addClass('is-valid');
    }
});

const current_datetime = new Date()

let formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
var Hari = ['','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];


function getRandomInt(min, max) {

    min = Math.ceil(min);

    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;

}



function passGenerate(length) {

   var result           = '';

   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

   var charactersLength = characters.length;

   for ( var i = 0; i < length; i++ ) {

      result += characters.charAt(Math.floor(Math.random() * charactersLength));

   }

   return result;

}

function sweetAlertDefault(html, type, timer ) {

      Swal.fire({

          html: html,

          icon: type,

          timer: timer,

          width: '350px',

          padding: '1em',

          position: 'top-end',

          showCancelButton: false,

          showConfirmButton: false

      });

  }



  function sweetAlertLoading(html, timer = null ) {

      Swal.fire({

          title: html,

          width: '350px',

          padding: '1em',

          position: 'top-end',

          showCancelButton: false,

          showConfirmButton: false,

          timer: timer,

          onBeforeOpen: () => {

              Swal.showLoading()

          }

      })

  }


function ajaxDeleteData(modul, id, data_table) {

    $.ajax({

        type: 'GET',

        url: "" + base_url + "/delete/"+modul+"/" + id,

        dataType: 'JSON',

        beforeSend: function () {

            sweetAlertLoading('Memproses');

        },

        success: function (data) {

            if (data.status == 'delete_successful') {

                sweetAlertDefault('<b>Data Berhasil Terhapus</b>', 'success', 2000 );

                data_table.ajax.reload(null, false);

            } else if (data.status == 'delete_failed') {

                sweetAlertDefault('<b>Gagal Hapus</b>', 'error', 2000 );

            } else {

                sweetAlertDefault('<b>Gagal Hapus (Kesalahan Sistem)</b>', 'error', 2000 );

            }



        },

        error: function (xmlhttprequest, textstatus, message) {

            sweetAlertDefault('<b>Koneksi Ke Server Gagal, Mohon Refresh Halaman</b>', 'error', 2000 );

        }

    });

  }


jQuery.validator.addMethod("exactlength", function(value, element, param) {

    return this.optional(element) || value.length == param;

}, $.validator.format("Please enter exactly {0} characters."));



jQuery.extend(jQuery.validator.messages, {

    required: '<i class="fa fa-pencil" style="color:red;"></i> Harus di isi.',

    remote: "Please fix this field xx.",

    email: "Masukan email dengan benar.",

    url: "Please enter a valid URL.",

    date: "Harus format tanggal.",

    dateISO: "Please enter a valid date (ISO).",

    number: "Harus berupa angka.",

    digits: "Please enter only digits.",

    creditcard: "Please enter a valid credit card number.",

    equalTo: "Please enter the same value again.",

    accept: "Please enter a value with a valid extension.",

    maxlength: jQuery.validator.format("Maksimal panjang huruf {0}."),

    minlength: jQuery.validator.format("Minimal panjang huruf {0}."),

    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),

    range: jQuery.validator.format("Please enter a value between {0} and {1}."),

    max: jQuery.validator.format("Tidak Lebih dari {0}."),

    min: jQuery.validator.format("Tidak Kurang dari {0}.")

});

function errorValidationLaravel(errors, targetProp) {

    var error_msg = ""

    var html = ""

    for (var key in errors) {

        if (errors.hasOwnProperty(key)) {

            error_msg += "<li>"+errors[key]+"</li>";

        }

    }


    if(errors){

        html += '<div class="alert alert-danger border-0 bg-danger alert-dismissible fade show" role="alert">'+

                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'

        html +='<ul class="text-white">'+error_msg+'</ul></div>'

        $(targetProp).empty().append(html)

    }

}

function delay(callback, ms) {

    var timer = 0;

    return function() {

        var context = this, args = arguments;

        clearTimeout(timer);

        timer = setTimeout(function () {

        callback.apply(context, args);

        }, ms || 0);

    };

}

function showPreviewImage(size, fromProp, targetProp) {



        if (fromProp.files[0].type == 'image/jpeg' || fromProp.files[0].type == 'image/png') {

            if (fromProp.files[0].size > size) {

                alert("Maksimal Ukuran File "+formatBytes(size));

                $(fromProp).val('');

            } else {

                var oFReader = new FileReader();

                oFReader.readAsDataURL(document.getElementById(fromProp.id).files[0]);

                oFReader.onload = function (oFREvent) {

                    document.getElementById(targetProp).src = oFREvent.target.result;

                }

            }

        } else {

            alert("Foto Harus berupa Gambar JPG/PNG");

            $(fromProp).val('');

        }



}

function formatBytes(bytes, decimals = 2) {

    if (bytes === 0) return '0 Bytes';



    const k = 1024;

    const dm = decimals < 0 ? 0 : decimals;

    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];



    const i = Math.floor(Math.log(bytes) / Math.log(k));



    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

}

function timeIndoFormat(data,type=null) {

    var res = data.split(" ");

    var tanggal = res[0].split("-");

    const months = ["Januari", "Februari", "Maret","April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    tanggal[1] = parseInt(tanggal[1]);

    let formatted_date = tanggal[2] + " " + months[tanggal[1]-1] + " " + tanggal[0]

    if(type==2){

        formatted_date = tanggal[2] + "-" + (tanggal[1]<=9 ? "0" : "")+tanggal[1]+ "-" + tanggal[0]

    }

    return formatted_date;

}

function disableEntry() {    
    $('.error').text('');            

    $('.form_custom').find('input').attr('readOnly', true);            
    $('.form_custom').find('input[type=number]').attr('readOnly', true);
    $('.form_custom').find('input[type=time]').attr('readOnly', true);
    $('.form_custom').find('input[type=email]').attr('readOnly', true);
    $('.form_custom').find('input[type=email]').attr('readOnly', true);    
    $('.form_custom').find('input[type=text]').attr('readOnly', true);    
    $('textarea').attr('readonly', 'readonly');

    $('.form_custom1').find('input').attr('readOnly', true);
    $('.form_custom1').find('input[type=number]').attr('readOnly', true);
    $('.form_custom1').find('input[type=time]').attr('readOnly', true);
    $('.form_custom1').find('input[type=email]').attr('readOnly', true);
    $('.form_custom1').find('input[type=email]').attr('readOnly', true);    
    $('.form_custom1').find('input[type=text]').attr('readOnly', true);

    $('.form_custom1').find('input').removeClass('is-valid is-invalid');
    $('.form_custom1').find('input[type=number]').removeClass('is-valid is-invalid');
    $('.form_custom1').find('input[type=time]').removeClass('is-valid is-invalid');
    $('.form_custom1').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom1').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom1').find('input[type=text]').removeClass('is-valid is-invalid');

}

function disableClearEntry() { 
    
    $('.error').text('');    
    
    $('.form_custom').find('input').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=number]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=time]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=text]').removeClass('is-valid is-invalid');

    $('.form_custom').find('input').attr('readOnly', true);
    $('.form_custom').find('input[type=number]').attr('readOnly', true);
    $('.form_custom').find('input[type=time]').attr('readOnly', true);
    $('.form_custom').find('input[type=email]').attr('readOnly', true);
    $('.form_custom').find('input[type=email]').attr('readOnly', true);
    $('.form_custom').find('input[type=text]').attr('readOnly', true);
    $('.form_custom').find('select').attr('disabled', true);    
    
    $('.form_custom').find('input').val('');
    $('.form_custom').find('input[type=number]').val('');
    $('.form_custom').find('input[type=time]').val('');
    $('.form_custom').find('input[type=email]').val('');
    $('.form_custom').find('input[type=email]').val('');
    $('.form_custom').find('input[type=text]').val('');    
}

function enableEntry() {    
    $('.error').text('');    

    $('.form_custom').find('input').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=number]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=time]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=email]').removeClass('is-valid is-invalid');
    $('.form_custom').find('input[type=text]').removeClass('is-valid is-invalid');
    $('.form_custom').find('select').removeClass('is-valid is-invalid');
    $('.form_custom').find('select').attr('disabled', true);        

    $('.form_custom1').find('input').attr('readOnly', false);
    $('.form_custom1').find('input[type=number]').attr('readOnly', false);
    $('.form_custom1').find('input[type=time]').attr('readOnly', false);
    $('.form_custom1').find('input[type=email]').attr('readOnly', false);
    $('.form_custom1').find('input[type=email]').attr('readOnly', false);
    $('.form_custom1').find('input[type=text]').attr('readOnly', false);
    $('.form_custom1').find('select').attr('disabled', false);    
    
    // $('.form_custom1').find('input').val('');
    // $('.form_custom1').find('input[type=number]').val('');
    // $('.form_custom1').find('input[type=time]').val('');
    // $('.form_custom1').find('input[type=email]').val('');
    // $('.form_custom1').find('input[type=email]').val('');
    // $('.form_custom1').find('input[type=text]').val('');    

    $('textarea').removeAttr('readonly');  
      
}

function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}

function clearSelect2(){    
    $(".single-select").val('').trigger('change');        
    $(".single-select2  ").val('').trigger('change');        
}

function clearModal() {
    $('.error').text('');
    $('.modal-body').find('input').removeClass('is-valid is-invalid');
    $('.modal-body').find('select').removeClass('is-valid is-invalid');
    $('.modal-body').find('single-select').removeClass('is-valid is-invalid');
    $('.modal-body').find('textarea').removeClass('is-valid is-invalid');

    $('.modal-body').find('.clearDisable').attr('disabled', false);
    // $('.modal-body').find('input[type=number]').attr('readOnly', false);
    // $('.modal-body').find('input[type=time]').attr('readOnly', false);
    // $('.modal-body').find('input[type=email]').attr('readOnly', false);
    // $('.modal-body').find('input[type=email]').attr('readOnly', false);
    // $('.modal-body').find('input[type=text]').attr('readOnly', false);

    $('.modal-body').find('select').val('');
    $('.modal-body').find('single-select').val("");
    $('.modal-body').find('textarea').val('');
    $('.modal-body').find('input[type=text]').val('');
    $('.modal-body').find('img').attr('src', 'img/dafault-img.png' );
    $('.modal-body').find('input[type=number]').val('');
    $('.modal-body').find('input[type=time]').val('');
    $('.modal-body').find('input[type=email]').val('');
    $('.modal-body').find('input[type=password]').val('');
    $('.modal-body').find('input[type=checkbox]').prop('checked',false);    
    $('.single-select').select2().select2('val', '');

}


function formatRupiah(angka, prefix){

	var number_string = angka.replace(/[^,\d]/g, '').toString(),

	split   		= number_string.split(','),

	sisa     		= split[0].length % 3,

	rupiah     		= split[0].substr(0, sisa),

	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);



	// tambahkan titik jika yang di input sudah menjadi angka ribuan

	if(ribuan){

		separator = sisa ? '.' : '';

		rupiah += separator + ribuan.join('.');

	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');

}

function convertToRupiah(angka)

{

	var rupiah = '';

	var angkarev = angka.toString().split('').reverse().join('');

	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';

	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');

}

function convertToRupiahNoRp(angka)

{

	var rupiah = '';

	var angkarev = angka.toString().split('').reverse().join('');

	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';

	return rupiah.split('',rupiah.length-1).reverse().join('');

}

function convertToAngka(rupiah)

{

	return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);

}

function BeforeSend(){
    $("#loading").show();                
    document.getElementById("btn_simpan").disabled = true;
    $("#transContent").addClass("overlay");
}

function AfterSend(){
    $("#loading").hide();
    document.getElementById("btn_simpan").disabled = false;
    $("#transContent").removeClass("overlay");
}

function titleCase(string) {

    var sentence = string.toLowerCase().split(" ");
    for(var i = 0; i< sentence.length; i++){
       sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }

    //document.write(sentence.join(" "));
    return sentence.join(' ');    
 }

 function konversiRomawi(nomor){

    var romawi = [12];
    var desimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    romawi = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
  
    var hasil = '';
  
    for(var index = 0; index < desimal.length; index++){
      while(desimal[index] <= nomor) {
        hasil += romawi[index];
        nomor -= desimal[index];
      }
    }
    return hasil;
  }

function BeforeSend(){
    $("#loading").show(); 
    var cells = document.getElementsByClassName("btn-action"); 
    for (var i = 0; i < cells.length; i++) { 
        cells[i].disabled = true;
    }                  
    $("#transContent").addClass("overlay");
}

function AfterSend(){
    $("#loading").hide();
    var cells = document.getElementsByClassName("btn-action"); 
    for (var i = 0; i < cells.length; i++) { 
        cells[i].disabled = false;
    }        
    $("#transContent").removeClass("overlay");
}
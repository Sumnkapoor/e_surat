var data_table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {
    
    //loaddata();        
});    

function loaddata() {        
    
    var action_url = "" + base_url + "/dashboard";            
    var action_type = "Cari";
                
    $.ajax({
        type: 'POST',
        url: action_url,
        dataType: 'JSON',
        data: {
            _token: CSRF_TOKEN,
            jenisSurat: jenisSurat,
            kodeSurat: kodeSurat
        },
        beforeSend: function(){
            BeforeSend();
        },
        complete: function(){
            AfterSend();
        },
        success: function (data) {
            if (data.status == 'insert_successful') {                                                
                success_noti('Berhasil ' + action_type + ' Data');
                $("#tgl_surat").val(data.tgl);
                $("#no_surat").val(data.nomerSurat);
                $("#id_surat_keluar").val(data.idSuratKeluar);
                $("#no_surat").prop("readonly", true);
                
            } else if (data.status == 'insert_failed') {
                error_noti('Gagal ' + action_type + ' Data'); 

                var errors = data.error;
                errorValidationLaravel(errors, '#error-validation');

            } else {
                error_noti('Gagal ' + action_type + ' (Kesalahan Sistem)');
            }
        },

        error: function (xmlhttprequest, textstatus, message) {
            error_noti('Koneksi Ke Server Gagal, '+message);
        }

    });            
    
}


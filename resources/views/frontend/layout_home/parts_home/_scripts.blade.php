<!-- Bootstrap JS -->
<script src="{{asset('bank_stiep/js/bootstrap.bundle.min.js') }}"></script>
<!--plugins-->
<script src="{{asset('bank_stiep/js/jquery.min.js') }}"></script>
<script src="{{asset('bank_stiep/js/jquery.validate.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/simplebar/js/simplebar.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/metismenu/js/metisMenu.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/chartjs/js/Chart.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/jquery-knob/excanvas.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/jquery-knob/jquery.knob.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/sweetalert2/dist/sweetalert2.js') }}"></script>
<!-- <script src="{{asset('bank_stiep/plugins/HTMLArea/htmlarea.js') }}"></script> -->
<script src="{{asset('bank_stiep/js/index.js') }}"></script>
<script src="{{asset('additional/js/global.js') }}"></script>
<script src="{{asset('bank_stiep/js/app.js') }}"></script>
<script src="{{asset('bank_stiep/js/plugins.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{asset('bank_stiep/js/pace.min.js') }}"></script>
<!-- <script src='https://cdn.tiny.cloud/1/5ash4v2w9rcd9tymhx1bssyqcagr7stjnbl86embpgajx2qw/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script> -->
<!-- Tabel -->
<script src="{{asset('bank_stiep/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>
<!-- Date Time -->
<script src="{{asset('bank_stiep/plugins/datetimepicker/js/legacy.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/datetimepicker/js/picker.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/datetimepicker/js/picker.time.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/datetimepicker/js/picker.date.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/bootstrap-material-datetimepicker/js/moment.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.min.js') }}"></script>

<!-- Notification -->
<script src="{{asset('bank_stiep/plugins/notifications/js/lobibox.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/notifications/js/notifications.min.js') }}"></script>
<script src="{{asset('additional/js/notification-custom-script.js') }}"></script>

<!-- AutoComplete -->
<script src="{{asset('bank_stiep/plugins/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
<!-- <script src="{{asset('bank_stiep/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script> -->

<!-- <script src="{{asset('bank_stiep/plugins/quill/quill.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/quill/quill.min.js') }}"></script>
<script src="{{asset('bank_stiep/plugins/quill/quill.core.js') }}"></script> -->

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
	
	$(document).ready(function () {			

		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');		
				
		$('.action').click(function(event){			      			      
			var urlx = $(this).attr('data-href');		      

			$.ajax({
				url: "" + base_url + urlx,
				type: 'get',
				cache: false,
				data: {
					_token: CSRF_TOKEN,                
				},									
				success: function (data) {					
					$('.isiContent').html(data.html);					
				},
		
				error: function (xhr, status, error, xmlhttprequest, textstatus, message) {						
					alert(xmlhttprequest+"/"+textstatus+"/"+message);																						
				}
			});				
		});									
		
	});	

</script>	

@stack('scripts')

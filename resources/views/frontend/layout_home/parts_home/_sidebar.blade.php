<div class="sidebar-header">
    <div>
        <img src="{{asset('img/logo_2.png')}}" style="width:50px;" alt="logo icon">
    </div>
    <div>
        <h4 class="logo-text">E-SURAT</h4>
    </div>
    <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
    </div>
</div>
<!--navigation-->
<ul class="metismenu" id="menu">
    @if(Session::get('admin') == 'y' )
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-cloud-drizzle'></i>
                </div>
                <div class="menu-title">SETUP</div>
            </a>
            <ul>
                <li> <a class="action" data-href="/departement"><i class="bx bx-arrow-from-left"></i>Departemen</a></li>            
                <li> <a class="action" data-href="/masterDisposisi"><i class="bx bx-arrow-from-left"></i>Disposisi</a></li>            
                <li> <a class="action" data-href="/jenisSurat"><i class="bx bx-arrow-from-left"></i>Jenis Surat</a></li>
                <li> <a class="action" data-href="/jenisSuratMasuk"><i class="bx bx-arrow-from-left"></i>Jenis Surat Masuk</a></li>            
            </ul>
        </li>    
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-clipboard'></i></div>
                <div class="menu-title">PROSES</div>
            </a>
            <ul>            
                <li> <a class="action" data-href="/suratMasuk"><i class="bx bx-arrow-from-left"></i>Surat Masuk</a></li>                        
                <li> <a class="action" data-href="/suratKeluar"><i class="bx bx-arrow-from-left"></i>Surat Keluar</a></li>                        
            </ul>
        </li>
    @else
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-clipboard'></i></div>
                <div class="menu-title">PROSES</div>
            </a>
            <ul>                
                <li> <a class="action" data-href="/suratKeluar"><i class="bx bx-arrow-from-left"></i>Surat Keluar</a></li>                        
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-clipboard'></i></div>
                <div class="menu-title">LAPORAN</div>
            </a>
            <ul>                
                <li> <a class="action" data-href="/lapSuratMasuk"><i class="bx bx-arrow-from-left"></i>Laporan Surat Masuk</a></li>                        
            </ul>
        </li>
    @endif
</ul>
<!--end navigation-->

<!doctype html>

<html lang="en" class="color-sidebar sidebarcolor1" >

<head>
	@include('frontend.layout_home.parts_home._head')
</head>

<body>
	<!--wrapper-->
	<div class="wrapper">
		<!--sidebar wrapper -->
		<div class="sidebar-wrapper" data-simplebar="true">
			@include('frontend.layout_home.parts_home._sidebar')
		</div>
		<!--end sidebar wrapper -->
		<!--start header -->
		<header>
			@include('frontend.layout_home.parts_home._topnav')
		</header>
		<!--end header -->
		<!--start page wrapper -->
		<div class="page-wrapper">
			<div class="page-content isiContent">
				@yield('content')
			</div>
		</div>
		<!--end page wrapper -->
		<!--start overlay-->
		<div class="overlay toggle-icon"></div>
		<!--end overlay-->
		<!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
		<!--End Back To Top Button-->
		<footer class="page-footer">
			<p class="mb-0">
				<marquee>
					UNIVERSITAS PGRI ADI BUANA SURABAYA || Jl. Dukuh Menanggal XII-4 Surabaya 60234, Telp. (031) 8281181
				</marquee>
			</p>
		</footer>
	</div>
	<!--end wrapper-->
	<!--start switcher-->
	<div class="switcher-wrapper">
		@include('frontend.layout_home.parts_home._setting')	
	</div>
	<!--end switcher-->
	@include('frontend.layout_home.parts_home._scripts')
	<script>
	
	</script>
</body>

</html>

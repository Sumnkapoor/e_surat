<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-primary">
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">  
                    <div class="invoice overflow-auto">          
                        <div class="table-responsive">
                            <!-- <button id="tambah" class="{{$data['btnClass']}}">{{$data['btnAdd']}}</button><br><br> -->
                            <table id="example2" class="table table-striped table-bordered" border="2">
                                
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Detail-->
    <div class="modal fade modal-form" id="exampleLargeModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="modal_label">Setting Surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form-horizontal form-label-left" id="form2" method="post">
                    <div class="modal-body">                
                        @csrf                                                            
                        <input type="hidden" class="form-control" id="method_field" name="_method" value="POST" />
                        <input type="hidden" class="form-control" id="kodeunit" value="" name="kodeunit">
                        <div id="error-validation"></div>
                        <div class="row g-2">
                            <div class="col-12">
                                <label for="inputPhoneNo" class="form-label"><b>Unit Surat</b></label>                            
                                <select class="{{$data['classFormSelect2']}}" name="unit_surat" id="unit_surat" width="100%">
                                    <option value="">[Unit Surat ?]</option>                                                        
                                    <option value="y" >Ya</option>
                                    <option value="n" >Tidak</option>                                    
                                </select>                                                    
                                <label for="unit_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>                        
                            <div class="col-12">
                                <label for="inputPhoneNo" class="form-label"><b>Kode Surat</b></label>                            
                                <input type="text" class="form-control border-start-0" id="kode_surat" name="kode_surat" placeholder="Kode Surat" />
                                <label for="kode_surat" generated="true" class="error"></label>
                                <label id="validationError"></label>
                            </div>                        
                        </div>                    
                    </div>
                    <div class="modal-footer">                    
                        <button type="button" id="btn_simpan" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('additional/js/departement.js') }}"></script>



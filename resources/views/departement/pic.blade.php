<!--breadcrumb-->
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
            </ol>
        </nav>
    </div>
</div>
<hr/>
<div class="card border-top border-0 border-4 border-warning">
    <div class="card-header bg-primary py-3">        
        <div class="row">
            <div class="col-md-1" >
                <img src="{{ URL::asset(session("logoHeaderTransaksi")) }}" width="100px" alt="" />                                       
            </div>
            <div class="col-md-11">
                <br>
                <h2 class="text-white">{{$data['subtitle']}}</h2>
                <h6 class="text-white">{{$data['alamatKampus']}}</h6>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="border p-3 rounded">
            <div id="invoice">            
                <div class="invoice">
                    
                    <main>
                        <button id="tambah" class="{{$data['btnClass']}}">{{$data['btnAdd']}}</button><br><br>
                        <!-- <table id="example2" class="table table-striped table-bordered" border="2">
                            
                        </table> -->

                        <table id="example2" class="table table-striped table-bordered">
                            <thead>
                                <tr align="center">
                                    <th>Aksi</th>
                                    <th>Nip</th>
                                    <th>Nama</th>                        
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10%" align="center">                            
                                        <button class="btn btn-outline-danger btn-delete"> <i class="bx bx-trash me-0"></i></button>
                                    </td>
                                    <td width="25%">123456</td>
                                    <td width="65%">Nama 1</td>                        
                                </tr>
                                <tr>
                                    <td width="10%" align="center">                            
                                        <button class="btn btn-outline-danger btn-delete"> <i class="bx bx-trash me-0"></i></button>
                                    </td>
                                    <td>789011</td>
                                    <td>Nama 2</td>                        
                                </tr>                                    
                            </tbody>
                        </table>
                    </main>                        
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-form" id="exampleLargeModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_label">Form Tambah PIC</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="form-horizontal form-label-left" id="form" method="post">
                <div class="modal-body">                
                    @csrf
                    <input type="hidden" class="form-control" id="method_field" name="_method" value="POST" />
                    <input type="hidden" class="form-control" id="id" value="" name="id">
                    <div id="error-validation"></div>
                    <div class="row g-2">
                        <div class="col-12">
                            <label for="inputPhoneNo" class="form-label"><b>Nama</b></label>                            
                            <select class="{{$data['classFormSelect2']}}" name="id_pgw" id="id_pgw" width="100%">
                                <option value=""></option>                                                        
                                <option value="1" >123456 - Nama 1</option>
                                <option value="1" >789011 - Nama 2</option>
                                <option value="1" >654321 - Nama 3</option>
                                <option value="1" >129876 - Nama 4</option>
                                <option value="1" >675478 - Nama 5</option>                                    
                            </select>                                
                            <label for="id_pgw" generated="true" class="error"></label>
                            <label id="validationError"></label>
                        </div>                        
                    </div>
                    
                </div>
                <div class="modal-footer">                    
                    <button type="button" id="btn_simpan" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('additional/js/pic_departement.js') }}"></script>

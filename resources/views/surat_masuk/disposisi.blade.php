<!--breadcrumb-->
<div id="transContent">
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3">{{$data['head']}}</div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$data['title']}}</li>
                </ol>
            </nav>
        </div>
    </div>
    <hr/>
    <div class="card border-top border-0 border-4 border-warning">
        <div class="card-header bg-primary py-3">        
            <div class="row">
                <div class="col-md-1" >
                    <img src="{{ URL::asset(session("logoHeaderTransaksi")) }}" width="100px" alt="" />                                       
                </div>
                <div class="col-md-11">
                    <br>
                    <h2 class="text-white">{{$data['subtitle']}}</h2>
                    <h6 class="text-white">{{$data['alamatKampus']}}</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="border border-primary p-3 rounded">
                <div id="invoice">            
                    <div class="invoice">
                        <main>
                            <form class="form-horizontal form-label-left" id="formEntry" method="post">    
                                @csrf                                                    
                                <input type="hidden" class="{{$data['classFormControl']}}" id="method_field" name="_method" value="POST" />                                                            
                                <input type="hidden" class="{{$data['classFormControl']}}" id="idTr" value="{{$dataDisposisi[0]->suratdisposisiid ?? ''}}" name="idTr">                                    
                                <input type="hidden" class="{{$data['classFormControl']}}" id="suratmasukid" value="{{$data['id']}}" name="suratmasukid">                                    
                                
                                    <div class="row form_custom"> 
                                        <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal Terima</label>
                                                <input type="text" class="form-control datepicker" id="tgl_terima" value="{{$dataDisposisi[0]->suratdisposisitglterima ?? ''}}" name="tgl_terima" >
                                                <label for="tgl_terima" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Nomor Disposisi</label>
                                                <input type="text" class="{{$data['classFormControl']}}" id="nomer_disposisi" value="{{ $dataDisposisi[0]->suratdisposisinomoragenda ?? $data['noDisposisi'] }}" name="nomer_disposisi" readonly>
                                                <label for="nomer_disposisi" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Nama Pengirim</label>
                                                <input type="text" class="{{$data['classFormControl']}}" id="nama_pengirim" value="{{$dataDisposisi[0]->suratdisposisinamapengirim ?? ''}}" name="nama_pengirim" >
                                                <label for="nama_pengirim" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">No. Surat</label>
                                                <input type="text" class="{{$data['classFormControl']}}" id="no_surat" value="{{$dataDisposisi[0]->suratdisposisinosurat ?? ''}}" name="no_surat" >
                                                <label for="no_surat" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tanggal Surat</label>
                                                <input type="text" class="{{$data['classFormControl']}} datepicker" id="tgl_surat" value="{{$dataDisposisi[0]->suratdispossisitglsurat ?? ''}}" name="tgl_surat" >
                                                <label for="tgl_surat" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Sifat</label>
                                                <select class="{{$data['classFormSelect2']}}" name="sifat" id="sifat" width="100%">
                                                    <option value="">[ Pilih Sifat Surat ]</option>                                                        
                                                    <option value="Biasa" {{( 'biasa' == ($dataDisposisi[0]->suratdisposisisifat ?? '')) ? 'selected' : '' }}>Biasa</option>
                                                    <option value="Penting" {{( 'penting' == ($dataDisposisi[0]->suratdisposisisifat ?? '')) ? 'selected' : '' }}>Penting</option>                                             
                                                    <option value="Rahasia" {{( 'rahasia' == ($dataDisposisi[0]->suratdisposisisifat ?? '')) ? 'selected' : '' }}>Rahasia</option>       
                                                    <option value="Segera" {{( 'segera' == ($dataDisposisi[0]->suratdisposisisifat ?? '')) ? 'selected' : '' }}>Segera</option>                                                                                                
                                                </select>                        
                                                <label for="sifat" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-12">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Perihal</label>
                                                <!-- <textarea id="mytextarea" name="mytextarea">Hello, World!</textarea> -->
                                                <textarea class="{{$data['classFormControl']}}" id="perihal" name="perihal" rows="5" cols="80">{{$dataDisposisi[0]->suratdisposisiperihal ?? ''}}</textarea>
                                                <label for="perihal" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>                                            
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-12">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Disposisi</label>
                                                @if (!$LMasterDisposisi)
                                                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                                    <div class="text-white">Master Disposisi Belum Ada</div>
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                                @else
                                                <select class="{{$data['classFormSelect2']}}" name="jenis_disposisi" id="jenis_disposisi" width="100%">
                                                    <option value="">[Pilih Disposisi]</option>
                                                    @foreach($LMasterDisposisi as $masterDisposisi)
                                                    <option value="{{ $masterDisposisi->disposisiid }}" {{($masterDisposisi->disposisiid == ($dataDisposisi[0]->iddisposisi ?? '')) ? 'selected' : '' }}>{{ ucwords(strtolower($masterDisposisi->disposisinama)) }}</option>
                                                    @endforeach                                                                                                    
                                                </select>                        
                                                @endif
                                                <label for="jenis_disposisi" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-12">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Catatan</label>
                                                <textarea class="{{$data['classFormControl']}}" id="isi" name="isi" rows="2">{{$dataDisposisi[0]->suratdisposisiisi ?? ''}}</textarea>
                                                <label for="perihal" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>                                                
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Kepada</label>
                                                @if (!$LdataPegawai)
                                                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                                    <div class="text-white">Master Pegawai Belum Ada</div>
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                                @else
                                                <select class="{{$data['classFormSelect2']}}" name="kepada" id="kepada" width="100%">
                                                    <option value="">[Pilih Pegawai]</option>
                                                    @foreach($LdataPegawai as $dataPegawai)
                                                    <option value="{{ $dataPegawai->idpegawai }}" {{($dataPegawai->idpegawai == ($dataDisposisi[0]->suratdisposisikepada ?? '')) ? 'selected' : '' }}>{{ ucwords(strtolower($dataPegawai->userdesc)) }}</option>
                                                    @endforeach                                                                                                    
                                                </select>
                                                @endif
                                                <label for="kepada" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Dari</label>                                                
                                                <input type="text" class="{{$data['classFormControl']}}" id="dari_nama" value="{{Session::get('nama') ?? ''}}" name="dari_nama" readonly>
                                                <input type="hidden" class="{{$data['classFormControl']}}" id="dari" value="{{($dataDisposisi) ? $dataDisposisi[0]->suratdisposisidari : Session::get('idPgw') }}" name="dari" readonly>
                                                <label for="dari" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <!-- <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Dikembalikan ke</label>
                                                <input type="text" class="{{$data['classFormControl']}}" id="dikembalikan" value="{{$dataDisposisi[0]->suratdisposisikembali ?? ''}}" name="dikembalikan" >
                                                <label for="dikembalikan" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tgl</label>                                            
                                                <input type="text" class="{{$data['classFormControl']}} datepicker" id="tgl_dikembalikan" value="{{$dataDisposisi[0]->suratdisposisikembalitgl ?? ''}}" name="tgl_dikembalikan" >
                                                <label for="tgl_dikembalikan" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div> -->
                                        <div class="row">                                                                                         
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Diteruskan</label>
                                                @if (!$LdataDepartement)
                                                <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show">
                                                    <div class="text-white">Master Departement Belum Ada</div>
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                                @else
                                                <select class="{{$data['classFormSelect2']}}" name="dep_diteruskan" id="dep_diteruskan" width="100%">
                                                    <option value="">[Pilih Departement]</option>
                                                    @foreach($LdataDepartement as $dataDepartement)
                                                    <option value="{{ $dataDepartement->kodeunit }}" {{($dataDepartement->kodeunit == ($dataDisposisi[0]->kodeunitditeruskan ?? '')) ? 'selected' : '' }}>{{ ($dataDepartement->namaunit) }}</option>
                                                    @endforeach                                                                                                    
                                                </select>
                                                @endif
                                                <label for="dep_diteruskan" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Tgl</label>
                                                <input type="text" class="{{$data['classFormControl']}} datepicker" id="tgl_diteruskan" value="{{$dataDisposisi[0]->suratdisposisiditeruskantgl ?? ''}}" name="tgl_diteruskan" >
                                                <label for="tgl_diteruskan" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>
                                        </div>
                                        <div class="row">                                                                                         
                                            <div class="col-md-12">
                                                <label for="inputCity" class="form-label" style="color:blue; font-weight:bold">Siap Simpan</label>
                                                <input type="text" class="{{$data['classFormControl']}} datepicker" id="tgl_siap_simpan" value="{{$dataDisposisi[0]->suratdisposisisiapsimpantgl ?? ''}}" name="tgl_siap_simpan" >
                                                <label for="tgl_siap_simpan" generated="true" class="error"></label>
                                                <label id="validationError"></label>
                                            </div>                                            
                                        </div>                                        
                                        <div class="row">                                                                                                                                         
                                            <div class="col-md-12" align="left">                                                                                                        
                                                <button type="button" id="btn_simpan" class="btn btn-success btn-sm"><i class='bx bx-save mr-1'></i>Simpan</button>
                                                <!-- <button type="button" id="btn_kembali" class="btn btn-danger btn-sm"><i class='bx bx-trash mr-1'></i>Kembali</button> -->
                                            </div>
                                        </div>                                        
                                    </div>

                            </form>
                        </main>                        
                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('additional/js/disposisi_masuk.js') }}"></script>
